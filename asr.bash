#!/bin/bash
# coding: utf-8

# Este script se ejecuta automáticamente al iniciar
# sesión también de manera automática el usuario "user"
# Se llama desde /home/user/.profile
# Debe copiarse en /home/user/

# Requiere partclone instalado
# apt-get install partclone

# Requiere que el usuario "user" pueda ejecutar partclone como root sin password
# apt-get install sudo
# visudo
#     user    ALL=NOPASSWD: ALL

# Suponemos que el disco esta así:
#  /dev/sda1 Partición SO Windows
#  /dev/sda2 Partición DATOS Windows
#  /dev/sda3 Partición SO Linux
#  /dev/sda4 Partición DATOS Linux (/home)

# Los backups estan en:
#     /home/user/backup/windows
#        sda1.win.so.ntfs.partclone.gz
#        sda2.win.datos.ntfs.partclone.gz
#    /home/user/backup/linux
#        sda3.linux.so.ext4.partclone.gz
#        sda4.linux.datos.ext4.partclone.gz


# Borramos la pantalla
clear

# Menu principal
# Nota muy importante!!!!!:
#   Si quieres quitar alguna opción borra la línea
#   Si solo la comentas da error
whiptail --title "Escoja una opción" \
         --menu "" \
         11 60 5 \
         --nocancel \
         --clear \
         0 "Reiniciar" \
         1 "Restaurar el sistema operativo Windows"  \
         2 "Restaurar la partición de DATOS de Windows"  \
       2>temp


# Han pulsado la tecla OK
if [ "$?" = "0" ];then
   # Miramos que opción han elegido
   _return=$(cat temp)

   # 0 "Reiniciar"
   if [ "$_return" = "0" ];then
      # Reiniciar el ordenador
      whiptail --title "Reiniciar el ordenador" \
               --clear \
               --msgbox "$(echo '      Pulse <Aceptar> para reiniciar el ordenador')" \
               11 60
      sudo init 6
   fi

   # 1 "Restaurar el sistema operativo Windows"
   if [ "$_return" = "1" ];then
        if (whiptail --title "Restaurar el sistema operativo Windows" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     11 60);then
           echo ""
           sudo gunzip -c backup/windows/sda2.win.so.ntfs.partclone.gz | sudo partclone.restore -o /dev/sda2 -N
        fi

        # Volvemos al menu principal
        bash asr.bash
   fi

   # 2 "Restaurar la partición de DATOS de Windows"
   if [ "$_return" = "2" ];then
        if (whiptail --title "Restaurar la partición de DATOS de Windows" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     11 60);then
           echo ""
           sudo gunzip -c backup/windows/sda3.win.datos.ntfs.partclone.gz | sudo partclone.restore -o /dev/sda3 -N
        fi

        # Volvemos al menu principal
        bash asr.bash
   fi


# Cancel or ESC
# Han pulsado la tecla CANCEL o ESC
else
   # Reiniciar el ordenador
   whiptail --title "Reiniciar el ordenador" \
            --clear \
            --msgbox "$(echo '      Pulse <Aceptar> para reiniciar el ordenador')" \
            11 60
#QUITAR EL # DEL "sudo init 6" CUANDO LO TENGAS TODO PROBADO
   #sudo init 6
fi


# Borramos la pantalla
clear

# Borramos el archivo temporal
rm -f temp
